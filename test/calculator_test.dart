import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:insulincalc/services/calculator.dart';
import 'package:insulincalc/services/sensor.dart';

void main() {
  Random random = Random(123);

  // random is not a good practice in tests ... don't try this at home kids
  group("Tests", () {
    test("range check should be valid for values 30 <= x <= 400", () {
      expect(Sensor.sensorValueSane(random.nextInt(370) + 30) , true);
    });
    test("range check should be invalid for values x < 30", () {
      expect(Sensor.sensorValueSane(random.nextInt(30)) , false);
    });
    test("range check should be invalid for values x > 400", () {
      expect(Sensor.sensorValueSane(random.nextInt(1000) + 401) , false);
    });
    test("ie shoudl be 1 here", () {
      expect(Calculator.calculateInsuline(160, 0, 120, 2.0, 30.0), 1);
    });
    test("ie shoudl be 61 here", () {
      expect(Calculator.calculateInsuline(160, 300, 120, 2.0, 30.0), 61);
    });
  });
}