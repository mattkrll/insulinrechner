import 'dart:math' show Random;

class Sensor {
  static final Random _random = new Random();

  static int getSensorValue() {
    return _random.nextInt(400) + 30;
  }

  static bool sensorValueSane(int sensorvalue) {
    return 30 <= sensorvalue && sensorvalue <= 400;
  }
}
