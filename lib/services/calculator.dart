import 'package:insulincalc/models/usersettings.dart';
import 'package:insulincalc/services/storage.dart';

class Calculator {
  Future<int> getCurrentInsulin(int carbs, int currentBloodSugar) async {
    final DateTime _dateTime = DateTime.now();
    final bool daytime = (_dateTime.hour >= 4 && _dateTime.hour < 22);

    final UserSettings currentSettings = await Storage.loadConfig();

    int targetBloodSugar;
    double insulinFactor;
    double correctionFactor;

    if (daytime) {
      targetBloodSugar = currentSettings.targetBloodsugarDay;
      insulinFactor = currentSettings.dayIF;
      correctionFactor = currentSettings.dayCF;
    } else {
      targetBloodSugar = currentSettings.targetBloodsugarNight;
      insulinFactor = currentSettings.nightIF;
      correctionFactor = currentSettings.nightCF;
    }

    return calculateInsuline(currentBloodSugar, carbs, targetBloodSugar,
        insulinFactor, correctionFactor);
  }

  static int calculateInsuline(int currentBloodSugar, int carbs,
      int targetBloodSugar, double insulinFactor, double correctionFactor) {
    int insulinForMeal = (insulinFactor * (carbs / 10)).floor();
    int insulinForCurrentSugar =
    ((currentBloodSugar - targetBloodSugar) / correctionFactor).floor();

    return insulinForMeal + insulinForCurrentSugar;
  }

}
