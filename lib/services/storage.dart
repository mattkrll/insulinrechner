import 'dart:io' show Directory,File;
import 'dart:convert' show jsonDecode,jsonEncode;
import 'package:insulincalc/models/default.usersettings.dart';
import 'package:insulincalc/models/usersettings.dart';
import 'package:path_provider/path_provider.dart' show getApplicationDocumentsDirectory;

class Storage {
  static Future<void> storeConfig(UserSettings config) async {
    _writeToConfigFile(jsonEncode(config));
  }

  static Future<UserSettings> loadConfig() async {
    try {
      return await _loadFromConfigFile()
          .whenComplete(() => print("Config loaded from file!"));
    } catch (e) {
      print("Config seems broken, creating new default");
      _createDefaultConfigFile()
          .whenComplete(() => print("Trying again with default config."));
      return await _loadFromConfigFile();
    }
  }

  static Future<void> _writeToConfigFile(String text) async {
    // print(text);
    final Directory directory = await getApplicationDocumentsDirectory();
    final File file = File('${directory.path}/config.json');
    await file
        .writeAsString(text)
        .whenComplete(() => print("Wrote to config file!"));
  }

  static Future<void> _createDefaultConfigFile() async {
    _writeToConfigFile(jsonEncode(defaultUserSettings))
        .whenComplete(() => print("Created default config!"));
  }

  static Future<UserSettings> _loadFromConfigFile() async {
    String configAsJSON;
    try {
      final Directory directory = await getApplicationDocumentsDirectory();
      final File file = File('${directory.path}/config.json');
      configAsJSON = await file.readAsString();
    } catch (e) {
      print("Couldn't read config file.");
      await _createDefaultConfigFile();
      return _loadFromConfigFile();
    }
    // print(configAsJSON);
    return UserSettings.fromJson(jsonDecode(configAsJSON));
  }
}
