import 'package:flutter/material.dart';
import 'package:insulincalc/models/usersettings.dart';

import 'package:insulincalc/screens/home.dart';
import 'package:insulincalc/screens/settings.dart';
import 'package:insulincalc/services/storage.dart';
import 'package:insulincalc/shared/loading.dart';

class StartWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Storage.loadConfig(),
        builder: (BuildContext context, AsyncSnapshot<UserSettings> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Center(child: Text(snapshot.error.toString()));
          } else if (!snapshot.hasData) {
            return Center(child: Loading());
          } else if (snapshot.data.insulinType == "") {
            return Settings();
          } else {
            return Home();
          }
        });
  }
}
