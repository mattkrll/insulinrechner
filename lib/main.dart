import 'dart:async';

import 'package:battery/battery.dart';
import 'package:flutter/material.dart';
import 'package:insulincalc/screens/battery.dart';
import 'package:insulincalc/screens/settings.dart';
import 'package:insulincalc/start_wrapper.dart';
import 'package:provider/provider.dart';

import 'models/DeviceInformationService.dart';


main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {


    return StreamProvider(
      builder: (BuildContext context) =>
      DeviceInformationService().batteryLevel,
      child: MaterialApp(
      title: 'Insulin Rechner',
      //home: BatteryView(),
      debugShowCheckedModeBanner: false,
      initialRoute: "/",
      routes: {
        "/": (context) => StartWrapper(),
        "/settings": (context) => Settings(),
        "/battery":(context) => MyBatteryPage(),

      },
    )
    );
  }
}





