class UserSettings {
  String insulinType;

  int targetBloodsugarDay;
  int targetBloodsugarNight;

  // Sugar correction factors
  double dayCF;
  double nightCF;

  // Insulin unit factors
  double dayIF;
  double nightIF;

  UserSettings(
      {this.insulinType,
      this.targetBloodsugarDay,
      this.targetBloodsugarNight,
      this.dayCF,
      this.nightCF,
      this.dayIF,
      this.nightIF});

  UserSettings.fromJson(Map<String, dynamic> json)
      : insulinType = json['insulinType'],
        targetBloodsugarDay = json['targetBloodsugarDay'],
        targetBloodsugarNight = json['targetBloodsugarNight'],
        dayCF = json['dayCF'],
        nightCF = json['nightCF'],
        dayIF = json['dayIF'],
        nightIF = json['nightIF'];

  Map<String, dynamic> toJson() => {
        'insulinType': insulinType,
        'targetBloodsugarDay': targetBloodsugarDay,
        'targetBloodsugarNight': targetBloodsugarNight,
        'dayCF': dayCF,
        'nightCF': nightCF,
        'dayIF': dayIF,
        'nightIF': nightIF
      };
}
