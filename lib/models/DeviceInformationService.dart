import 'dart:async';
import 'package:battery/battery.dart';
import 'BatteryInformation.dart';

class DeviceInformationService {

  Stream<BatteryInformation> get batteryLevel => _batteryLevelController.stream;

  StreamController<BatteryInformation>_batteryLevelController =  StreamController<BatteryInformation>();


  bool _broadcastBattery = false;
  Battery _battery = Battery();

  Future _broadcastBatteryLevel() async {
    _broadcastBattery = true;
    while (_broadcastBattery) {
      var batteryLevel = await _battery.batteryLevel;
      _batteryLevelController
          .add(BatteryInformation(batteryLevel));
      await Future.delayed(Duration(seconds: 5));
    }
  }

  void stopBroadcast() {
    _broadcastBattery = false;
  }
}

