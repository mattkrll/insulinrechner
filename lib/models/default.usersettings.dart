final Map<String, dynamic> defaultUserSettings = {
  'insulinTyp': "",
  'targetBloodsugarDay': 120,
  'targetBloodsugarNight': 110,
  'dayCF': 30.0,
  'nightCF': 40.0,
  'dayIF': 2.0,
  'nightIF': 1.0,
};
