import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:insulincalc/models/usersettings.dart';
import 'package:insulincalc/services/calculator.dart';
import 'package:insulincalc/services/sensor.dart';
import 'package:insulincalc/services/storage.dart';
import 'package:insulincalc/shared/loading.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:async';
import 'package:battery/battery.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _formKey = GlobalKey<FormState>();

  int _insulinValue = 0;
  int _lastSensorValue = 0;

  int _carbs = 0;
  TextEditingController _controller = new TextEditingController();

  bool _showSensorError = false;
  bool _showBluetoothError = false;

  FlutterBlue _flutterBlue = FlutterBlue.instance;

  TextStyle settingsStyle =
      TextStyle(fontSize: 28, backgroundColor: Colors.amber);

  Future<void> _launchEmail(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Battery _battery = Battery();

  BatteryState _batteryState;
  StreamSubscription<BatteryState> _batteryStateSubscription;

   int batteryLevel=0;
  String messageStatus(int state) {
    if(state>=50)

      return 'Dont worry';
    if(state >10)
      return 'You need a charge';
    if(state >0)
      return 'batery is low';
    else
      throw 'I dont know';
  }

  Future<void> _batteryLevel() async {
    int x = await _battery.batteryLevel;
    print(x);
    setState(() {
      batteryLevel = x;
    });
  }

  @override
  void initState() {
    super.initState();
    _batteryLevel();
    _batteryStateSubscription =
        _battery.onBatteryStateChanged.listen((BatteryState state) {
          setState(() {
            _batteryState = state;
          });
        });

    _battery.batteryLevel.then((value) => (){
        batteryLevel = value;
        print(value);


    });

  }

  @override
  Widget build(BuildContext context) {
    _batteryLevel();
    return FutureBuilder(
        future: Storage.loadConfig(),
        builder: (BuildContext context, AsyncSnapshot<UserSettings> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Center(child: Text(snapshot.error.toString()));
          } else if (!snapshot.hasData) {
            return Center(child: Loading());
          } else {
            // print(jsonEncode(snapshot.data.toJson()));
            return Scaffold(
              appBar: AppBar(
                actions: <Widget>[
                  IconButton(
                    onPressed: () {
                      Navigator.pushNamed(context, "/settings");
                    },
                    icon: Icon(Icons.settings),
                    tooltip: "Einstellungen",
                  ),
                  //IconButton(
                  //  onPressed: () {
                  //    Navigator.pushNamed(context, "/battery");
                  //  },
                  //  icon: Icon(Icons.battery_std),
                  //  tooltip: "Battery",
                  // ),
                ],
              ),
              backgroundColor: Colors.white,
              body: Container(
                padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),

                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Flexible(
                              child: TextFormField(
                                keyboardType: TextInputType.numberWithOptions(
                                    signed: false, decimal: false),
                                textCapitalization: TextCapitalization.words,
                                controller: _controller,
                                obscureText: false,

                                // controller: _isSearching,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(0.0),
                                  border: InputBorder.none,
                                  hintText: "Kohlenhydrate",
                                ),
                              ),
                            ),
                            Padding(
                                padding: new EdgeInsets.only(left: 10),
                                child: IconButton(
                                  onPressed: () => {
                                    showDialog(
                                        context: context,
                                        builder: (context) {
                                          return new SimpleDialog(
                                              children: <Widget>[
                                                Column(
                                                  children: [
                                                    Image.asset(
                                                      'assets/images/curious.jpg',
                                                      height: 150,
                                                      width: 100,
                                                    ),
                                                    Padding(
                                                        padding:
                                                            EdgeInsets.all(15)),
                                                    Text(
                                                      "1. Kohlenhydrate eingeben \n\n"
                                                      "2. Auf den Insulin berechnen klicken",
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ],
                                                ),
                                              ]);
                                        }),
                                  },
                                  icon: new Icon(
                                    Icons.help,
                                    color: Colors.grey,
                                    size: 26,
                                  ),
                                )),
                          ]),
                      RaisedButton(
                          onPressed: () async {
                            setState(() {
                              _showSensorError = false;
                              _showBluetoothError = false;
                            });
                            
                            final bool bluetoothIsOn = await _flutterBlue.isOn;
                            
                            final int newSensorValue = Sensor.getSensorValue();
                            final int newInsulinValue = await Calculator()
                                .getCurrentInsulin(int.parse(_controller.text),
                                    newSensorValue);

                            if (bluetoothIsOn == false) {
                              setState(() {
                                _showBluetoothError = true;
                              });
                              
                            } else if (Sensor.sensorValueSane(newSensorValue)) {
                              setState(() {
                                _insulinValue = newInsulinValue;
                                _lastSensorValue = newSensorValue;
                              });
                            } else {
                              setState(() {
                                _showSensorError = true;
                                _insulinValue = 0;
                                _lastSensorValue = 0;
                              });
                            }
                          },
                          child: Text(
                            "Insulin berechnen",
                            style: TextStyle(color: Colors.black),
                          )),

                      SizedBox(
                        height: 20.0,
                      ),

                      Text(
                          "Insulin Typ: " +
                              snapshot.data.insulinType.toString(),
                          style: settingsStyle),
                      Text(
                          "Zielblutzucker Tag: " +
                              snapshot.data.targetBloodsugarDay.toString(),
                          style: settingsStyle),
                      Text(
                          "Zielblutzucker Nacht: " +
                              snapshot.data.targetBloodsugarNight.toString(),
                          style: settingsStyle),
                      Text(
                          "Insulin Faktor Tag: " +
                              snapshot.data.dayIF.toString(),
                          style: settingsStyle),
                      Text(
                          "Insulin Faktor Nacht: " +
                              snapshot.data.nightIF.toString(),
                          style: settingsStyle),
                      Text(
                          "Korrektur Faktor Tag: " +
                              snapshot.data.dayCF.toString(),
                          style: settingsStyle),
                      Text(
                          "Korrektur Faktor Nacht: " +
                              snapshot.data.nightCF.toString(),
                          style: settingsStyle),
                      SizedBox(
                        height: 20.0,
                      ),

                      Text(
                        () {
                          if (_showBluetoothError) {
                            return "Bluetooth einschalten";
                          } else {
                            return _showSensorError
                                ? "Sensorwert ist ungewöhnlich, bitte überprüfen Sie Ihren Sensor"
                                : "";
                          }
                        }(),
                        style: TextStyle(
                            fontSize: 32,
                            color: Colors.red,
                            backgroundColor: Colors.black),
                      ),

                      SizedBox(
                        height: 20.0,
                      ),

                      Text(
                        "Letzter Messwert: " + _lastSensorValue.toString(),
                        style: TextStyle(
                            fontSize: 32, backgroundColor: Colors.lightGreen),
                      ),
                      Text(
                        "IE zu geben: " + _insulinValue.toString(),
                        style: TextStyle(
                            fontSize: 32, backgroundColor: Colors.lightGreen),

                      ),
                      batteryLevel<=10?Text( // si bateria es menor a 10 /
                          "Achtung! Akkustand: " + batteryLevel.toString() +"% Bitte aufladen.", //
                          style: TextStyle(
                              fontSize: 32, backgroundColor: Colors.red)):Text(""),

                    ],
                  ),
                ),
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return new SimpleDialog(children: <Widget>[
                          Column(
                            children: [
                              Padding(padding: EdgeInsets.all(15)),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Bei Fragen, Anregungen oder Problemen können Sie uns jederzeit kontaktieren.",
                                  style: TextStyle(fontSize: 14),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async =>
                                    {_launchEmail("mailto:mail@insulinum.de")},
                                child: Text(
                                  "mail@insulinium.de",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.blue),
                                ),
                              ),
                            ],
                          ),
                        ]);
                      });
                },
                child: Icon(Icons.mail),
                backgroundColor: Colors.blue,
              ),
            );
          }
        });
  }
}
