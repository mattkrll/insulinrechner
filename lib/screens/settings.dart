import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:insulincalc/models/usersettings.dart';
import 'package:insulincalc/services/storage.dart';
import 'package:insulincalc/shared/constants.dart';
import 'package:insulincalc/shared/loading.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final _formKey = GlobalKey<FormState>();

  UserSettings _currentSettings;

  @override
  void initState() {
    super.initState();
    Storage.loadConfig().then((loadedConfig) => {
          setState(() {
            _currentSettings = loadedConfig;
          })
        });
  }

  TextStyle fieldDescriptionStyle = TextStyle(fontSize: 18);

  //@TODO BUILD PROPER INPUT VALIDATORS

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Storage.loadConfig(),
        builder: (BuildContext context, AsyncSnapshot<UserSettings> snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Center(child: Text(snapshot.error.toString()));
          } else if (!snapshot.hasData) {
            return Center(child: Loading());
          } else {
            // print(jsonEncode(snapshot.data.toJson()));
            return Scaffold(
              appBar: AppBar(
                  title: Text(
                    "Daten eingeben",
                  ),
                  actions: <Widget>[
                    IconButton(
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return new SimpleDialog(children: <Widget>[
                                Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        "Geben Sie hier Ihre Daten ein, bevor Sie mit der Berechnung beginnen. Scrollen Sie dann herunter und drücken Sie auf 'Speichern'.",
                                        style: TextStyle(fontSize: 16),
                                      ),
                                    ),
                                  ],
                                ),
                              ]);
                            });
                      },
                      icon: Icon(Icons.help),
                      tooltip: "Einstellungen",
                    ),
                  ]),
              body: Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 20.0, horizontal: 50.0),
                child: Form(
                  key: _formKey,
                  child: Scrollbar(
                    child: ListView(
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),

                        TextFormField(
                          initialValue:
                              snapshot.data.insulinType,
                          decoration: textInputDecoration.copyWith(
                              hintText: "Insulin Typgit "),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(() => _currentSettings.insulinType = val);
                          },


                        ),
                        Text("Insulin Type", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue:
                              snapshot.data.targetBloodsugarDay.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Ziel Blutzucker Tag"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(() => _currentSettings.targetBloodsugarDay =
                                int.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: false),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                        ),
                        Text("Zielblutzucker Tag", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue:
                              snapshot.data.targetBloodsugarNight.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Ziel Blutzucker Nacht"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(() => _currentSettings
                                .targetBloodsugarNight = int.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: false),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                        ),
                        Text("Zielblutzucker Nacht",style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue: snapshot.data.dayCF.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Korrektur Faktor Tag"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(
                                () => _currentSettings.dayCF = double.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: true),
                        ),
                        Text("Korrektur Faktor Tag", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue: snapshot.data.nightCF.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Korrektur Faktor Nacht"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(() =>
                                _currentSettings.nightCF = double.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: true),
                        ),
                        Text("Korrektur Faktor Nacht", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue: snapshot.data.dayIF.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Insulinfaktor Tag"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(
                                () => _currentSettings.dayIF = double.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: true),
                        ),
                        Text("Insulinfaktor Tag", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        TextFormField(
                          initialValue: snapshot.data.nightIF.toString(),
                          decoration: textInputDecoration.copyWith(
                              hintText: "Insulinfaktor Nacht"),
                          validator: (val) => val.isEmpty ? "Name" : null,
                          onChanged: (val) {
                            setState(() =>
                                _currentSettings.nightIF = double.parse(val));
                          },
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: true),
                        ),
                        Text("Insulinfaktor Nacht", style: fieldDescriptionStyle,),
                        SizedBox(
                          height: 20.0,
                        ),
                        RaisedButton(
                              onPressed: () async {
                        if (_formKey.currentState.validate()) {
                        Storage.storeConfig(_currentSettings).whenComplete(
                        () => Navigator.pushNamedAndRemoveUntil(
                        context, "/", (_) => false));
                        }
                        },
                          child: Text(
                            "Speichern",
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        });
  }
}
