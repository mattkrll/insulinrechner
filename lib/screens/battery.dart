import 'dart:async';

import 'package:flutter/material.dart';
import 'package:battery/battery.dart';

class MyBatteryPage extends StatefulWidget {
  MyBatteryPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyBatteryPageState createState() => _MyBatteryPageState();
}

class _MyBatteryPageState extends State<MyBatteryPage> {
  Battery _battery = Battery();

  BatteryState _batteryState;
  StreamSubscription<BatteryState> _batteryStateSubscription;

  @override
  void initState() {
    super.initState();
    _batteryStateSubscription =
        _battery.onBatteryStateChanged.listen((BatteryState state) {
      setState(() {
        _batteryState = state;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Plugin example batery'),
      ),
      body: Center(
        child: Text('$_batteryState'),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.battery_unknown),
        onPressed: () async {
          final int batteryLevel = await _battery.batteryLevel;
          // ignore: unawaited_futures
          showDialog<void>(
            context: context,
            builder: (_) => AlertDialog(
              content: 
              Container(
                height: 150,
                child: Column(
                
                children: [Text('Battery: $batteryLevel%'),
              Text(messageStatus(batteryLevel)),
              ],),),
              
              actions: <Widget>[
                FlatButton(
                  child: const Text('OK'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          );
        },
      ),
    );
  }

  String messageStatus(int state) {
    if(state>=50)
      
        return 'Dont worry';
    if(state >10)
        return 'You need a charge';
     if(state >0)
        return 'batery is low';
    else
        throw 'I dont know';
    }
  

  @override
  void dispose() {
    super.dispose();
    if (_batteryStateSubscription != null) {
      _batteryStateSubscription.cancel();
    }
  }
}
